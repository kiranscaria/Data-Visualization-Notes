# Data Visualization

This is a collection of blog posts, codes, books and everything I found useful on Data Visualization.

## Getting Started 
The programming language is Python and usually most of the tools you will find here are also python based.

## Requirements
* Python 3 development environment
* Numpy
* Pandas
* Matplotlib
* Seaborn

## Installation

### Ubuntu
>pip install numpy matplotlib pandas seaborn 

### Anaconda 
>conda install numpy seaborn pandas matplotlib
